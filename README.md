# ra1nbow

Easily customize recovery mode with checkm8

## Download/Usage

Download and usage instructions can be found [here](https://devluke.gitlab.io/ra1nbow/).

## Requirements

1. A checkm8 supported iOS device
   - Must be in pwned DFU mode
   - Must have signature checks removed
   - Must have patched iBSS/iBEC

2. A lightning to USB-A cable
   - Third-party cables may or may not work properly

3. A computer that can run bash
   - Must have [libirecovery](https://github.com/libimobiledevice/libirecovery) installed
   - Can not be a virtual machine
