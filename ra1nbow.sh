#!/bin/bash

red=0
green=0
blue=0
alpha=1

image_path=

rbhelp () {
	echo
	echo "==> ra1nbow functions:"
	echo "=>   - rbhelp                            Prints this list"
	echo "=>   - rbcurrent [color|image]           Prints the current rgba, image path, or both"
	echo "=>   - rbreset [color|image]             Resets the bg color, image, or both"
	echo "=>   - rbcolor <r> <g> <b> [a]           Sets the bg color to the specified rgb(a)"
	echo "=>   - rbfade <r> <g> <b> [a] [frames]   Fades the bg color to the specified rgb(a)"
	echo "=>   - rbpulse <r> <g> <b> [a] [frames]  Pulses the bg color to the specified rgb(a)"
	echo "=>   - rbrainbow [frames]                Fades the bg color to each color of the rainbow"
	echo "=>   - rbimage <path>                    Sets the image to the specified path" # not started
	echo
}

rbcurrent () {
	rgba="rgba($red, $green, $blue, $alpha)"
	if [ "$1" = color ]; then
		echo $rgba
	elif [ "$1" = image ]; then
		echo $image_path
	else
		echo "=> color rgba:  $rgba"
		echo "=> image path:  $image_path"
	fi
}

rbreset () {
	if [ "$1" = color ]; then
		rbcolor 0 0 0 1
	elif [ "$1" = image ]; then
		rbimage
	else
		rbcolor 0 0 0 1
		rbimage
	fi
}

rbcolor () {
	real_red=
	real_green=
	real_blue=
	red=$(bc -l <<< $1)
	green=$(bc -l <<< $2)
	blue=$(bc -l <<< $3)
	if [ -z "$4" ]; then
		alpha=1
		real_red=$1
		real_green=$2
		real_blue=$3
	elif [ "$4" = 0 ]; then
		alpha=0
		real_red=0
		real_green=0
		real_blue=0
	else
		alpha=$(bc -l <<< $4)
		real_red=$(bc -l <<< $red*$alpha)
		real_green=$(bc -l <<< $green*$alpha)
		real_blue=$(bc -l <<< $blue*$alpha)
	fi
	irecovery -c "bgcolor $real_red $real_green $real_blue"
}

rbfade () {
	frames=$([ ! -z "$5" ] && echo $5 || echo 50)
	red_transition=$(bc -l <<< $1-$red)
	red_increment=$(bc -l <<< $red_transition/$frames)
	green_transition=$(bc -l <<< $2-$green)
	green_increment=$(bc -l <<< $green_transition/$frames)
	blue_transition=$(bc -l <<< $3-$blue)
	blue_increment=$(bc -l <<< $blue_transition/$frames)
	alpha_transition=0
	alpha_increment=0
	if [ ! -z "$4" ]; then
		alpha_transition=$(bc -l <<< $4-$alpha)
		alpha_increment=$(bc -l <<< $alpha_transition/$frames)
	fi
	for i in {1..$frames}; do
		new_red=$(bc -l <<< $red+$red_increment)
		new_green=$(bc -l <<< $green+$green_increment)
		new_blue=$(bc -l <<< $blue+$blue_increment)
		new_alpha=$(bc -l <<< $alpha+$alpha_increment)
		rbcolor $new_red $new_green $new_blue $new_alpha
	done
	rbcolor $1 $2 $3 $4
}

rbpulse () {
	original_red=$red
	original_green=$green
	original_blue=$blue
	original_alpha=$alpha
	rbfade $1 $2 $3 $4 $5
	sleep .5
	rbfade $original_red $original_green $original_blue $original_alpha $5
}

rbrainbow () {
	rbfade 255 0 0 1 $1
	rbfade 255 127 0 1 $1
	rbfade 255 255 0 1 $1
	rbfade 0 255 0 1 $1
	rbfade 0 0 255 1 $1
	rbfade 75 0 130 1 $1
	rbfade 139 0 255 1 $1
}

rbimage () {}

rbreset
echo "==> Imported ra1nbow functions. Type rbhelp for a list of functions."
